
/**
 * When a module is installed on a boat, a ModInstall configuration is created
 */
export interface ModInstall {
    id?: number;
    boatId?: number;
    modId?: number;
    installedAt?: Date;
    removedAt?: Date;
    organisationId: number;
    modSerial: string;
    alertNotificationList: AlertNotification[];
    externalSensorConfigList: ExternalSensorConfig[];
    engineStealCableUsage?: EngineStealCableUsageEnum;
    batteryCharacteritics: BatteryCharacteritics;
    geofencingRelayActivation: number;
    keysafeMode: number;
    schockProfil?: SchockProfilEnum;
}

/**
 * Part of ModInstall model, used to register alert notification targets
 */
export interface AlertNotification {
    id: number;
    sms: boolean;
    email: boolean;
    staff: boolean;
    pushNotification: boolean;
}

/**
 * Used to describe an external sensor:
 * id: A, sensorType: 1 ==> NMEA Gateway
 */
export interface ExternalSensorConfig {
    id: string;
    sensorType: string;
}

/**
 * Describe usage done for alarm cable
 */
export enum EngineStealCableUsageEnum {
    NOT_USED,
    ENGINE_STEAL,
    ENGINE_HEAT,
    INSURANCE_PACK
}

/**
 * Used to describe boat service battery.
 * When defined, a new property "BATTERY_CHARGE_LEVEL" is added on each Nauticsafe module message
 */
export interface BatteryCharacteritics{
    voltage: number;
    decreasingLoad: number;
    tensionAtOrigin: number;
}

/**
 * Define a schock configuration (Used in Backoffice at first)
 */
export enum SchockProfilEnum{
    SCHOCK_PROFILE_IN_CALM_SEA_NAVIGATION,
    SCHOCK_PROFILE_TURBULENT_SEA, 
    SCHOCK_PROFILE_WINTERING,
    SCHOCK_PROFILE_AFLOAT,
    SCHOCK_PROFILE_DISABLED,
    SCHOCK_PROFILE_SPORT_MODE
}