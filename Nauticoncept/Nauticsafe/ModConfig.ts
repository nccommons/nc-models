export interface ModConfig {
    modSerial: string;
    createdAt: Date;
    numConfig: number;
    firmwareVersion?: number;
    encodedConfig?: string;
    parsedConfig: ModParsedConfig;
}

export interface ModParsedConfig {
    DATE_HEURE_GMT: Date,
    MOD_SERIAL: number,
    MODULE_STATUS: number,
    FIRMWARE_VERSION: number,
    CONFIG_VERSION: number,
    NB_BLE_CONN: number,
    ALERT_SERVITUDE_LOW: number,
    ALARM_SERVITUDE_LOW: number,
    ALERT_TENSION_ENGINE_LOW: number,
    ALARM_TENSION_ENGINE_LOW: number,
    ALERT_TEMPERATURE_LOW: number,
    ALERT_TEMPERATURE_HIGH: number,
    ALERT_SCHOCK_X: number,
    ALERT_SCHOCK_Y: number,
    ALERT_SCHOCK_Z: number,
    ALERT_HEEL_LOW_X: number,
    ALERT_HEEL_HIGH_X: number,
    ALERT_HEEL_LOW_Y: number,
    ALERT_HEEL_HIGH_Y: number,
    ALERT_HEEL_LOW_Z: number,
    ALERT_HEEL_HIGH_Z: number,
    ALERT_INTERNAL_MODULE_TENSION_LOW: number,
    RELAY: number,
    ENGINE_STEAL: number,
    ALERT_CIRCUIT_BREAKER_ACTIVE: number,
    SILENT_MODE: number,
    TOR_ACTIVATE_RUN: number,
    NAUTISOCKET_STATE: number,
    NMEA_ENGINE: number,
    STAY_ALIVE_MODULE_TENSION: number,
    GPS_COLLECT_FREQUENCY: number, // V36
    GEO_RELAI: number,// V37 ?
    RELAY_USE: number,
}