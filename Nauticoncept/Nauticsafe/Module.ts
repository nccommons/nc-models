export interface Module{
    id: string;
    organisationId: number;
    // organisationName: string;
    modSerial: string;
    created_at: Date;
    saleStatus: string;
    description: string;
    fullSerial: string;
}