declare module "Nauticoncept/Nauticsafe/ModConfig" {
    export interface ModConfig {
        modSerial: string;
        createdAt: Date;
        numConfig: number;
        firmwareVersion?: number;
        encodedConfig?: string;
        parsedConfig: ModParsedConfig;
    }
    export interface ModParsedConfig {
        DATE_HEURE_GMT: Date;
        MOD_SERIAL: number;
        MODULE_STATUS: number;
        FIRMWARE_VERSION: number;
        CONFIG_VERSION: number;
        NB_BLE_CONN: number;
        ALERT_SERVITUDE_LOW: number;
        ALARM_SERVITUDE_LOW: number;
        ALERT_TENSION_ENGINE_LOW: number;
        ALARM_TENSION_ENGINE_LOW: number;
        ALERT_TEMPERATURE_LOW: number;
        ALERT_TEMPERATURE_HIGH: number;
        ALERT_SCHOCK_X: number;
        ALERT_SCHOCK_Y: number;
        ALERT_SCHOCK_Z: number;
        ALERT_HEEL_LOW_X: number;
        ALERT_HEEL_HIGH_X: number;
        ALERT_HEEL_LOW_Y: number;
        ALERT_HEEL_HIGH_Y: number;
        ALERT_HEEL_LOW_Z: number;
        ALERT_HEEL_HIGH_Z: number;
        ALERT_INTERNAL_MODULE_TENSION_LOW: number;
        RELAY: number;
        ENGINE_STEAL: number;
        ALERT_CIRCUIT_BREAKER_ACTIVE: number;
        SILENT_MODE: number;
        TOR_ACTIVATE_RUN: number;
        NAUTISOCKET_STATE: number;
        NMEA_ENGINE: number;
        STAY_ALIVE_MODULE_TENSION: number;
        GPS_COLLECT_FREQUENCY: number;
        GEO_RELAI: number;
        RELAY_USE: number;
    }
}
declare module "Nauticoncept/Nauticsafe/ModInstall" {
    /**
     * When a module is installed on a boat, a ModInstall configuration is created
     */
    export interface ModInstall {
        id?: number;
        boatId?: number;
        modId?: number;
        installedAt?: Date;
        removedAt?: Date;
        organisationId: number;
        modSerial: string;
        alertNotificationList: AlertNotification[];
        externalSensorConfigList: ExternalSensorConfig[];
        engineStealCableUsage?: EngineStealCableUsageEnum;
        batteryCharacteritics: BatteryCharacteritics;
        geofencingRelayActivation: number;
        keysafeMode: number;
        schockProfil?: SchockProfilEnum;
    }
    /**
     * Part of ModInstall model, used to register alert notification targets
     */
    export interface AlertNotification {
        id: number;
        sms: boolean;
        email: boolean;
        staff: boolean;
        pushNotification: boolean;
    }
    /**
     * Used to describe an external sensor:
     * id: A, sensorType: 1 ==> NMEA Gateway
     */
    export interface ExternalSensorConfig {
        id: string;
        sensorType: string;
    }
    /**
     * Describe usage done for alarm cable
     */
    export enum EngineStealCableUsageEnum {
        NOT_USED = 0,
        ENGINE_STEAL = 1,
        ENGINE_HEAT = 2,
        INSURANCE_PACK = 3
    }
    /**
     * Used to describe boat service battery.
     * When defined, a new property "BATTERY_CHARGE_LEVEL" is added on each Nauticsafe module message
     */
    export interface BatteryCharacteritics {
        voltage: number;
        decreasingLoad: number;
        tensionAtOrigin: number;
    }
    /**
     * Define a schock configuration (Used in Backoffice at first)
     */
    export enum SchockProfilEnum {
        SCHOCK_PROFILE_IN_CALM_SEA_NAVIGATION = 0,
        SCHOCK_PROFILE_TURBULENT_SEA = 1,
        SCHOCK_PROFILE_WINTERING = 2,
        SCHOCK_PROFILE_AFLOAT = 3,
        SCHOCK_PROFILE_DISABLED = 4,
        SCHOCK_PROFILE_SPORT_MODE = 5
    }
}
declare module "Nauticoncept/Nauticsafe/Module" {
    export interface Module {
        id: string;
        organisationId: number;
        modSerial: string;
        created_at: Date;
        saleStatus: string;
        description: string;
        fullSerial: string;
    }
}
